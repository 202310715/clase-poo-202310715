﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace herencia
{
    class Herencia
    {
        public int atributo1;
        private int atributo2;
        protected int atributo3;

        public void metodo1()
        {

        }
        private void metodo2()
        {

        }

        protected void metodo3()
        {

        }

        class Hijo: Herencia
        {
            
        }
    }
}
