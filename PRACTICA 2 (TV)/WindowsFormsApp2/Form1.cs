﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            tv TV1 = new tv();//Tamaño
            TV1.setTamanio(30);
            int tamanioTv1 = TV1.getTamanio();
            MessageBox.Show("El tamaño de la Tv1 es: " + tamanioTv1.ToString() + " Pulgadas");

            TV1.setVolumen(20);//Volumen
            int volumenTv1 = TV1.getVolumen();
            MessageBox.Show("El volumen de la Tv1 es: " + volumenTv1.ToString());

            TV1.setColor("Gris");//Color
            string colorTv1 = TV1.getColor();
            MessageBox.Show("El color de la Tv1 es: " + colorTv1);


            TV1.setBrillo(98);//Brillo
            int brilloTv1 = TV1.getBrillo();
            MessageBox.Show("El brillo de la Tv1 es: " + brilloTv1.ToString());

            TV1.setContraste(70);//Contraste
            int contrasteTv1 = TV1.getContraste();
            MessageBox.Show("El contraste de la Tv1 es: " + contrasteTv1.ToString());

            TV1.setMarca("LG");//Marca
            string marcaTv1 = TV1.getMarca();
            MessageBox.Show("El Marca de la Tv1 es: " + marcaTv1);

           








        }
    }
}
