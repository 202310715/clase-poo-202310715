﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN
{
    class CPU
    {
        public string marca = "";
        public string color = "";
        public string procesador = "";
        public int procesadores = 0;
        public int ram = 0;
        public string almacenamiento = "";
     

        public void setmarca(string marca)
        {
            this.marca = marca;
        }

        public string getmarca()
        {
            return this.marca;
        }

        public void setColor(string color)
        {
            this.color = color;
        }

        public string getColor()
        {
            return this.color;
        }


        public void setprocesador(string procesador)
        {
            this.procesador = procesador;
        }

        public string getprocesador()
        {
            return this.procesador;
        }

        public void setprocesadores(int procesadores)
        {
            this.procesadores = procesadores;
        }

        public int getprocesadores()
        {
            return this.procesadores;
        }

        public void setram(int ram)
        {
            this.ram = ram;
        }

        public int getram
            ()
        {
            return this.ram;
        }


        public void setalmacenamiento(string almacenamiento)
        {
            this.almacenamiento = almacenamiento;
        }

        public string getalmacenamiento
            ()
        {
            return this.almacenamiento;
        }



    }
}
