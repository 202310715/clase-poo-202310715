﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXAMEN
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CPU cpu1 = new CPU();
            cpu1.setmarca("Asus");
            string marcacpu1 = cpu1.getmarca();
            MessageBox.Show("La marca del CPU es " + marcacpu1);

            cpu1.setColor("Negra");
            string Colorcpu1 = cpu1.getColor();
            MessageBox.Show("El color del gabinete es: " + Colorcpu1);

            cpu1.setprocesador("AMD Ryzen 7 3800X");
            string procesadorcpu1 = cpu1.getprocesador();
            MessageBox.Show("Su procesador es un:  " + procesadorcpu1);


            cpu1.setprocesadores(8);
            int procesadorescpu1 = cpu1.getprocesadores();
            MessageBox.Show("El numero de procesadores es: " + procesadorescpu1.ToString());

            cpu1.setram(16);
            int ramcpu1 = cpu1.getram();
            MessageBox.Show("La memoria ram instalada es de: " + ramcpu1.ToString());

            cpu1.setalmacenamiento("2 TERABYTES");
            string almacenamientocpu1 = cpu1.getalmacenamiento();
            MessageBox.Show("Tiene un disco duro de: " + almacenamientocpu1);
        }
    }
}
