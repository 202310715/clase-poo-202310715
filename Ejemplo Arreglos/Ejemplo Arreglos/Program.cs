﻿using System;

namespace Ejemplo_Arreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] catalogoNumeros = new int[10];


            catalogoNumeros[0] = 1;
            for (int i = 0; i < catalogoNumeros.Length; i++)
            {
                Console.WriteLine("Ingresa un numero");
                int num = Convert.ToInt32( Console.ReadLine());
                catalogoNumeros[i] = num;
                
            }
            foreach (int val in catalogoNumeros)
            {
                Console.WriteLine(val);
            }
            Console.WriteLine(catalogoNumeros[0]);

        }
    }
}
